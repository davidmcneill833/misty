# Spring 19 Senior Project
We built a framework for deriving features from images and using those 
features to train a machine learning model (WAC specifically) to map from
those features to a given label (like a color)

We built a dataset and dataset format. You can generate datasets in this same
format using a script we wrote. The format is also outlined in this documentation

## Using the model
Models can be generated using test-vgg-wac.py. This script takes about
3 minutes to run on my laptop. It will output the training and testing
accuracy. It will also give the accuracy for each classifier in the WAC model that is trained. The model is pickled into a file called testModel.pickle

You can load the model using pickle.load(filename). This will instantiate
a LanguageModel object.

## About the model
The model is of type LanguageModel which is a class we wrote. It can easily be used to predict the label of an image by using the predictImageWord function. Please review the doc strings in LanguageModel to review usage.

The model is essentially a pipeline of VGG to WAC. The images are prepared by the model to be passed to VGG to generate a set of features for the given image. Then those features are fed to WAC which produces a guess at the word/label for the image.

It is easy to swap out VGG for another feature generater. You can edit the ImageFeatureGenerator class to add support for doing that.

## Dataset
The dataset is stored as a csv. It contains 4 columns. 

- img (Full image encoded as base64 string)
- label (word label for object in image)
- dimensions (height and width of the full image)
- crop (bounding box of the object in the full image)

The dataset contains images with objects, their bounding boxes, and their labels.
Each image is stored as a base64 encoded string that can be decoded into
a numpy array. The dimensions are stored as a string that represents an array. The easiast way to load and view the dataset is to use the functions
from CS481Dataset.py to load the dataset and decode the images. There is some preprocessing of the pandas Dataframe that is loaded from the csv
to be done before you can use the dataset, and all of that is taken care of if you use the CS481Dataset.py module functions.

## Creating new datasets
You can create new datasets using the cozmo-data-collection.py script.

## Scripts
+ fastai dependency is broken. Requires 0.7.0 version. 

- LanguageModel.py (wac / ImageFeatureGen / CS481Dataset): uses VGG19 as feature generator for images; applies WAC to those images. 
- cozmo-color-identification.py (LanguageModel / Model): [does this sort of already do what I want to do with a live-running color Language Model???]
- cozmo-object-detection.py (Model): live-running code that creates bounding box around objects. 
- cozmo-data-collection.py (Model / CS481Dataset): "Do you want to save this image?"
- convertDataset.py (CS481Dataset): add column indicting pos examples; randomly choose negative examples "FOR EACH COLOR" equal to the # of positives. 
- ImageFeatureGen.py (CS481Dataset / keras..VGG19): Resizes image & passes it to pre-trained vgg19; returns 2nd to last layer as 1000-length feature array.  
- CS481Dataset.py (PIL): encodes and decodes data from base64, saved in .csv format 
- Model.py: (fastai / PIL) object detection & bounding box image size conversion. 
- wac.py: WAC model code. Train & predict and add new utterances. 
- test-vgg-wac.py: test-run through pre-collected dataset, grounding images using color vocabulary.
